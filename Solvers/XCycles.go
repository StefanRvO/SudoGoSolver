package Solvers

import (
	"SudoGoSolver/CellCandidates"
	"SudoGoSolver/Solvers/ChainFinder"
)

type XCycles struct{}

func (s XCycles) SetCandidates(b *Board) {
	chain_finder := ChainFinder.ChainFinder{}
	chain_finder.SetCandidates(s, b, chain_finder.GetAllLinks)
}

func (s XCycles) getAllCycles(c *CCandidates, prev *CCandidates, chain *map[*CCandidates]ChainNode, visited map[*CCandidates]ChainNode) []map[*CCandidates]ChainNode {
	visited[c] = (*chain)[c]

	cycles := make([]map[*CCandidates]ChainNode, 0)

	for link, linktype := range (*chain)[c].Links {
		if link == prev {
			continue
		}
		if prev != nil {
			prev_linktype := visited[prev].Links[c]
			if prev_linktype == ChainFinder.Weak && linktype == ChainFinder.Weak {
				continue
			}
			if prev_linktype == ChainFinder.Strong && linktype == ChainFinder.Strong {
				continue
			}
		}

		visited_copy := make(map[*CCandidates]ChainNode)
		for k, v := range visited {
			visited_copy[k] = v
		}
		v := visited_copy[c]
		v.Next = link
		visited_copy[c] = v

		if _, ok := visited[link]; !ok {
			cycles = append(cycles, s.getAllCycles(link, c, chain, visited_copy)...)
		} else {
			// start_linktype := visited[link].Links[visited[link].Next]

			// if linktype == ChainFinder.Weak && start_linktype == ChainFinder.Weak {
			// 	continue
			// }
			// if linktype == ChainFinder.Strong && start_linktype == ChainFinder.Strong {
			// 	continue
			// }

			cycles = append(cycles, visited_copy)
		}
	}
	return cycles
}

// Remove nodes in chain with just a single neighbour
// Do this repeadetly untill only nodes with multiple neighbours are left
// The remaining nodes will form cycles in the chain
func (s XCycles) pruneChainEnds(chain map[*CCandidates]ChainNode) map[*CCandidates]ChainNode {
	removedAny := true
	for removedAny {
		removedAny = false
		for c, _ := range chain {
			node := chain[c]
			for link := range node.Links {
				if _, ok := chain[link]; !ok {
					delete(node.Links, link)
					removedAny = true
				}
			}
			if len(node.Links) <= 1 {
				delete(chain, c)
				removedAny = true
			} else {
				chain[c] = node
			}
		}
	}
	return chain
}

func (s XCycles) isCyclePerfect(cycle *map[*CCandidates]ChainNode) bool {
	for _, v := range *cycle {
		cnt := make(map[ChainFinder.LinkType]int)
		if len(v.Links) != 2 {
			return false
		}
		for _, linktype := range v.Links {
			cnt[linktype]++
		}
		if len(cnt) != 2 {
			return false
		}
	}
	return true
}

func (s XCycles) evenNumberElimination(b *Board, cycles map[*CCandidates]ChainNode, val uint8) int {
	eliminated := 0
	for i := range b.Candidates {
		c := &b.Candidates[i]
		if _, ok := cycles[c]; ok || c.IsSolved() || !c.IsCandidate(val) {
			continue
		}
		for _, g := range CellCandidates.UnitGetters {
			cells := make(map[*CCandidates]bool)
			for _, cell_in_view := range *g(c) {
				if _, ok := cycles[cell_in_view]; ok {
					cells[cell_in_view] = true
				}
			}
			if len(cells) >= 2 {
				c.RemoveCandidate(val)
				eliminated++
				break
			}
		}
	}
	return eliminated
}

func (s XCycles) OddNumberElimination(b *Board, cycles map[*CCandidates]ChainNode, val uint8) int {
	var n *CCandidates = nil
	for k := range cycles {
		n = k
		break
	}

	searched := make(map[*CCandidates]bool)
	prev := n
	for {
		next := cycles[n].Next

		if prev != n {
			prev_linktype := cycles[prev].Links[n]
			next_linktype := cycles[n].Links[next]
			if prev_linktype == ChainFinder.Strong && next_linktype == ChainFinder.Strong {
				n.SetCandidate(val)
				return 1
			} else if prev_linktype == ChainFinder.Weak && next_linktype == ChainFinder.Weak {
				n.RemoveCandidate(val)
				return 1
			}
		}

		if _, ok := searched[next]; ok {
			return 0
		}
		searched[next] = true
		prev = n
		n = next
	}
}

func (s XCycles) SearchChain(b *Board, chain *map[*CCandidates]ChainNode, val uint8) {
	for n := range *chain {
		pruned := s.pruneChainEnds(*chain)
		cycles := s.getAllCycles(n, nil, &pruned, make(map[*CCandidates]ChainNode))

		for _, cycle := range cycles {
			cycle = s.pruneChainEnds(cycle)
			if len(cycle) <= 2 {
				continue
			}
			if len(cycle)%2 == 0 && s.isCyclePerfect(&cycle) {
				if s.evenNumberElimination(b, cycle, val) > 0 {
					return
				}
			} else if len(cycle)%2 == 1 {
				if s.OddNumberElimination(b, cycle, val) > 0 {
					return
				}
			}
		}
	}
}
