package Solvers

import (
	"SudoGoSolver/CellCandidates"
)

type YWing struct{}

func (s YWing) SetCandidates(b *Board) {
	candidate_cnt := b.GetCandidateCount()
	for i := range b.Candidates {
		pivot := &b.Candidates[i]
		if pivot.GetCandidateCount() != 2 {
			continue
		}
		s.tryPivot(pivot, b)
		if b.GetCandidateCount() != candidate_cnt {
			return
		}
	}
}

func (s YWing) tryPivot(c *CCandidates, b *Board) {
	candidate_cnt := b.GetCandidateCount()

	cand := c.GetCandidates()
	reverse_cand := c.GetCandidates()
	ReverseSlice(reverse_cand)

	for i1, g1 := range UnitGetters {
		for i2, g2 := range UnitGetters {
			if i1 != i2 {
				s.tryPivotUnits(c, g1, g2, cand)
				if b.GetCandidateCount() != candidate_cnt {
					return
				}
				s.tryPivotUnits(c, g1, g2, reverse_cand)
				if b.GetCandidateCount() != candidate_cnt {
					return
				}
			}
		}
	}
}

func has2Candidates(c *CCandidates) bool {
	return c.GetCandidateCount() == 2
}

func (s YWing) tryPivotUnits(c *CCandidates, u1, u2 UnitGetter, candidates []uint8) {
	unit1 := CellCandidates.FilterGroup(CellCandidates.GetUnitLinks(c, candidates[0], u1), has2Candidates)
	unit2 := CellCandidates.FilterGroup(CellCandidates.GetUnitLinks(c, candidates[1], u2), has2Candidates)

	if len(unit1) != 1 {
		return
	}
	if len(unit2) != 1 {
		return
	}

	p1 := unit1[0]
	p2 := unit2[0]

	if p1 == p2 {
		return
	}

	shared_candidates := p1.Candidates & ^(1 << candidates[0])
	if !p2.IsCandidates(shared_candidates) || c.IsCandidates(shared_candidates) {
		return
	}

	pinchers := CandidateGroup{p1, p2}
	for _, cell := range CellCandidates.GetCellsWithCandidates(p1.GetCommonCells(p2), shared_candidates) {
		if !CellCandidates.GroupContains(pinchers, cell) {
			cell.RemoveCandidates(shared_candidates)
		}
	}
}
