package Solvers

import (
	"SudoGoSolver/CellCandidates"

	"golang.org/x/exp/maps"
)

type NakedGroup struct{}

func (s NakedGroup) SetCandidates(b *Board) {
	SetCandidates(s, b)
}

func (s NakedGroup) SetCandidatesInGroupN(group *CandidateGroup, group_size int) {
	pairs := make(map[uint32]map[int]bool)
	for i, c := range *group {
		if c.GetCandidateCount() >= 2 && c.GetCandidateCount() <= group_size {
			if _, ok := pairs[c.Candidates]; !ok {
				pairs[c.Candidates] = make(map[int]bool)
			}
			pairs[c.Candidates][i] = true
		}
	}

	for i, c := range *group {
		if c.GetCandidateCount() >= 2 && c.GetCandidateCount() <= group_size {
			for k, _ := range pairs {
				newVal := (k | c.Candidates)
				if CellCandidates.GetCandidateCount(newVal) <= group_size {
					if _, ok := pairs[newVal]; !ok {
						pairs[newVal] = make(map[int]bool)
					}
					pairs[newVal][i] = true
					maps.Copy(pairs[newVal], pairs[k])
				}
			}
		}
	}

	for k, v := range pairs {
		if len(v) != group_size || CellCandidates.GetCandidateCount(k) != group_size {
			delete(pairs, k)
		}
	}

	for k, v := range pairs {
		for i := 0; i < 9; i++ {
			if _, ok := v[i]; !ok {
				(*group)[i].RemoveCandidates(k)
			}
		}
	}
}

func (s NakedGroup) SetCandidatesInGroup(group *CandidateGroup) {
	s.SetCandidatesInGroupN(group, 2)
	s.SetCandidatesInGroupN(group, 3)
	s.SetCandidatesInGroupN(group, 4)
}
