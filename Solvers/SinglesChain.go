package Solvers

import (
	"SudoGoSolver/Solvers/ChainFinder"
	Solvers "SudoGoSolver/Solvers/ChainFinder"
)

type ChainNode = Solvers.ChainNode

type SinglesChainSolver struct{}

func (s SinglesChainSolver) SetCandidates(b *Board) {
	chain_finder := ChainFinder.ChainFinder{}
	chain_finder.SetCandidates(s, b, chain_finder.GetStrongLinks)
}

func (s SinglesChainSolver) purge(chain *map[*CCandidates]ChainNode, color int, val uint8) int {
	cnt := 0
	for k, v := range *chain {
		if v.Color == color {
			k.RemoveCandidate(val)
			cnt++
		}
	}
	return cnt
}

func (s SinglesChainSolver) checkColorTwiceInUnit(b *Board, chain *map[*CCandidates]ChainNode, val uint8) int {
	for node, _ := range *chain {
		for _, getter := range UnitGetters {
			color_cnt := make(map[int]int)

			for _, c := range *getter(node) {
				if n, ok := (*chain)[c]; ok {
					color_cnt[n.Color]++
					if color_cnt[n.Color] > 1 {
						return n.Color
					}
				}
			}
		}
	}
	return -1
}

func (s SinglesChainSolver) isAllColorsInAnyUnit(c *CCandidates, chain *map[*CCandidates]ChainNode) bool {
	color_cnt := make(map[int]int)
	for _, getter := range UnitGetters {
		for _, c := range *getter(c) {
			if v, ok := (*chain)[c]; ok {
				color_cnt[v.Color]++
				if len(color_cnt) == 2 {
					return true
				}
			}
		}
	}
	return false
}
func (s SinglesChainSolver) checkColorTwiceOutside(b *Board, chain *map[*CCandidates]ChainNode, val uint8) int {
	not_in_chain := make(CandidateGroup, 0, 9)

	for i := range b.Candidates {
		c := &b.Candidates[i]
		if !c.IsCandidate(val) || c.IsSolved() {
			continue
		}
		if _, ok := (*chain)[c]; !ok {
			not_in_chain = append(not_in_chain, c)
		}
	}

	candidate_cnt := b.GetCandidateCount()
	for _, n := range not_in_chain {
		if s.isAllColorsInAnyUnit(n, chain) {
			n.RemoveCandidate(val)
		}
	}
	return candidate_cnt - b.GetCandidateCount()
}

func (s SinglesChainSolver) SearchChain(b *Board, chain *map[*CCandidates]ChainNode, val uint8) {
	if color := s.checkColorTwiceInUnit(b, chain, val); color >= 0 {
		s.purge(chain, color, val)
		return
	}
	if s.checkColorTwiceOutside(b, chain, val) > 0 {
		return
	}
}
