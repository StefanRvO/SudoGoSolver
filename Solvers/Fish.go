package Solvers

import (
	"SudoGoSolver/CellCandidates"
	"SudoGoSolver/SudokuBoard"

	"gonum.org/v1/gonum/stat/combin"
)

type Fish struct {
	groupsize int
}

func GetGroupPermutations(g []CandidateGroup, group_size int) [][]CandidateGroup {
	var permutations [][]CandidateGroup

	for _, comb := range combin.Combinations(len(g), group_size) {
		var group []CandidateGroup
		for _, i := range comb {
			group = append(group, g[i])
		}
		permutations = append(permutations, group)
	}
	return permutations
}

func (s Fish) SetCandidates(b *Board) {
	for val := uint8(1); val <= 9; val++ {
		candidate_cnt := b.GetCandidateCount()
		s.fish(b, val, SudokuBoard.GetRows, CellCandidates.GetColumn)
		s.fish(b, val, SudokuBoard.GetColumns, CellCandidates.GetRow)
		if candidate_cnt != b.GetCandidateCount() {
			return
		}
	}
}

func (s Fish) fish(b *Board, val uint8, u1 UnitsGetter, u2 UnitGetter) {
	unitgroups := make([]CandidateGroup, 0)
	for _, unit := range u1(b) {
		cells := CellCandidates.GetCellsWithCandidates(unit, 1<<val)
		if len(cells) >= 2 && len(cells) <= s.groupsize {
			unitgroups = append(unitgroups, cells)
		}
	}

	if len(unitgroups) < s.groupsize {
		return
	}

	candidate_cnt := b.GetCandidateCount()

	for _, unitgroups := range GetGroupPermutations(unitgroups, s.groupsize) {
		cells := make(CandidateGroup, 0, 9)
		for _, g := range unitgroups {
			cells = append(cells, g...)
		}

		units := make(map[*CandidateGroup]int)
		for _, c := range cells {
			units[u2(c)]++
		}
		if len(units) == s.groupsize {
			for k := range units {
				for _, c := range *k {
					if !CellCandidates.GroupContains(cells, c) {
						c.RemoveCandidate(val)
					}
				}
			}
		}
		if candidate_cnt != b.GetCandidateCount() {
			return
		}
	}
}

type JellyFish struct{}

func (s JellyFish) SetCandidates(b *Board) {
	Fish{groupsize: 4}.SetCandidates(b)
}

type SwordFish struct{}

func (s SwordFish) SetCandidates(b *Board) {
	Fish{groupsize: 3}.SetCandidates(b)
}

type XWing struct{}

func (s XWing) SetCandidates(b *Board) {
	Fish{groupsize: 2}.SetCandidates(b)
}
