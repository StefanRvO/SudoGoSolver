package Solvers

import (
	"SudoGoSolver/CellCandidates"
)

type HiddenGroup struct{}

func (s HiddenGroup) SetCandidates(b *Board) {
	SetCandidates(s, b)
}

func (s HiddenGroup) SetCandidatesInGroupN(group *CandidateGroup, group_size int) {
	unSolvedCells := make(CandidateGroup, 0)
	unSolved := uint32(0)

	for _, c := range *group {
		if !c.IsSolved() {
			unSolvedCells = append(unSolvedCells, c)
			unSolved |= c.Candidates
		}
	}

	groups := CellCandidates.GetCandidatesPermutations(unSolved, group_size)
	pairs := make(map[uint32]CandidateGroup)

	for _, p := range groups {
		for _, c := range unSolvedCells {
			if (c.Candidates & p) > 0 {
				pairs[p] = append(pairs[p], c)
			}
		}
	}

	for k, v := range pairs {
		// Make sure we have the required number of sets
		if len(v) != group_size {
			delete(pairs, k)
			continue
		}

		// Make sure no member of the pair does not exist in any other cell
		for _, c := range *group {
			if !CellCandidates.GroupContains(v, c) && (c.Candidates&k) > 0 {
				delete(pairs, k)
				break
			}
		}
	}

	for k, v := range pairs {
		for _, c := range v {
			c.Set(c.Candidates & k)
		}
	}

}
func (s HiddenGroup) SetCandidatesInGroup(group *CandidateGroup) {
	s.SetCandidatesInGroupN(group, 2)
	s.SetCandidatesInGroupN(group, 3)
	s.SetCandidatesInGroupN(group, 4)
}
