package Solvers

import (
	"SudoGoSolver/CellCandidates"
)

type XYZWing struct{}

func (s XYZWing) SetCandidates(b *Board) {
	candidate_cnt := b.GetCandidateCount()
	for i := range b.Candidates {
		pivot := &b.Candidates[i]
		if pivot.GetCandidateCount() != 3 {
			continue
		}
		s.tryPivot(pivot, b)
		if b.GetCandidateCount() != candidate_cnt {
			return
		}
	}
}

func (s XYZWing) tryPivot(c *CCandidates, b *Board) {
	candidate_cnt := b.GetCandidateCount()

	for _, cand := range CellCandidates.GetCandidatePermutations(c.Candidates, 3) {
		for i1, g1 := range UnitGetters {
			for i2, g2 := range UnitGetters {
				if i1 != i2 {
					s.tryPivotUnits(c, g1, g2, cand)
					if b.GetCandidateCount() != candidate_cnt {
						return
					}
				}
			}
		}
	}
}

func (s XYZWing) tryPivotUnits(c *CCandidates, u1, u2 UnitGetter, candidates []uint8) {
	unit1 := CellCandidates.GetUnitLinks(c, candidates[0], u1)
	unit1 = CellCandidates.FilterGroup(unit1, has2Candidates)
	unit1 = CellCandidates.GetCellsWithCandidates(unit1, 1<<candidates[2])

	unit2 := CellCandidates.GetUnitLinks(c, candidates[1], u2)
	unit2 = CellCandidates.FilterGroup(unit2, has2Candidates)
	unit2 = CellCandidates.GetCellsWithCandidates(unit2, 1<<candidates[2])

	if len(unit1) != 1 {
		return
	}
	if len(unit2) != 1 {
		return
	}

	p1 := unit1[0]
	p2 := unit2[0]

	if p1 == p2 {
		return
	}

	pinchers := CandidateGroup{p1, p2}

	pivot_cells := c.GetCommonCells(p1)
	for _, cell := range CellCandidates.GetCellsWithCandidates(p1.GetCommonCells(p2), 1<<candidates[2]) {

		if !CellCandidates.GroupContains(pivot_cells, cell) {
			continue
		}
		if !CellCandidates.GroupContains(pinchers, cell) && cell != c {
			cell.RemoveCandidate(candidates[2])
		}
	}
}
