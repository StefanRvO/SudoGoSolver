package Solvers

import (
	"SudoGoSolver/CellCandidates"
)

type BoxLineReduction struct{}

func (s BoxLineReduction) SetCandidates(b *Board) {
	for i := 0; i < 9; i++ {
		s.SetCandidatesInGroup(&b.Rows[i])
		s.SetCandidatesInGroup(&b.Columns[i])
	}
}

func isgroupInSameBox(group *CandidateGroup) bool {
	for i := 1; i < len(*group); i++ {
		if (*group)[0].GetBox() != (*group)[i].GetBox() {
			return false
		}
	}
	return true
}

func (s BoxLineReduction) SetCandidatesInGroup(group *CandidateGroup) {
	for val := uint8(1); val <= 9; val++ {
		cells := CellCandidates.GetCellsWithCandidates(*group, 1<<val)
		if len(cells) <= 1 {
			continue
		}

		if isgroupInSameBox(&cells) {
			CellCandidates.RemoveCandidatesFromGroup(*cells[0].GetBox(), cells, 1<<val)
		}
	}
}
