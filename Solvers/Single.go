package Solvers

type NakedSingle struct{}

func (s NakedSingle) SetCandidates(b *Board) {
	SetCandidates(s, b)
}

func (s NakedSingle) SetCandidatesInGroup(group *CandidateGroup) {
	for i := 0; i < 9; i++ {
		val := (*group)[i].GetValue()
		if val == 0 {
			continue
		}
		for j := 0; j < 9; j++ {
			if i != j {
				(*group)[j].RemoveCandidate(val)
			}
		}
	}
}

type HiddenSingle struct{}

func (s HiddenSingle) SetCandidates(b *Board) {
	SetCandidates(s, b)
}

func (s HiddenSingle) SetCandidatesInGroup(group *CandidateGroup) {
	for val := uint8(1); val <= 9; val++ {
		cnt := 0
		foundIndex := 0
		for i := 0; i < 9; i++ {
			if (*group)[i].IsCandidate(val) {
				cnt++
				foundIndex = i
			}
		}
		if cnt == 1 && !(*group)[foundIndex].IsSolved() {
			(*group)[foundIndex].SetCandidate(val)
		}
	}
}
