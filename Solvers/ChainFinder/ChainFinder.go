package ChainFinder

import (
	"SudoGoSolver/CellCandidates"
	"SudoGoSolver/SudokuBoard"
	"fmt"
)

type ChainFinder struct {
}

type LinkType int

const (
	Weak = iota
	Strong
)

func (l LinkType) String() string {
	return [...]string{"Weak", "Strong"}[l]
}

type ChainNode struct {
	Color int
	Links map[*CCandidates]LinkType
	Next  *CCandidates
}

func GetCycleString(cycle *map[*CCandidates]ChainNode) string {
	if len(*cycle) == 0 {
		return "<>"
	}
	//Start at some node
	var n *CCandidates = nil
	for k, _ := range *cycle {
		n = k
		break
	}
	searched := make(map[*CCandidates]bool)
	str := ""
	searched[n] = true

	for {
		str += fmt.Sprintf("<%s:%d>", n.String(), (*cycle)[n].Color)
		next := (*cycle)[n].Next
		if next == nil {
			return fmt.Sprint(str, "ERROR:nil, len:", len(*cycle), cycle)
		}
		linktype := (*cycle)[n].Links[next]
		if _, ok := searched[next]; !ok {
			searched[next] = true
			str += fmt.Sprintf("--%s-->", linktype.String())
			n = next
		} else {
			str += fmt.Sprintf("--%s--><%s:%d>", linktype.String(), next.String(), (*cycle)[next].Color)
			break
		}
	}
	return str
}

func (s ChainNode) String() string {
	str := fmt.Sprintf("color: %d, links(", s.Color)
	for link, linktype := range s.Links {
		str += fmt.Sprint("(", link, linktype, ")")
	}
	str += ") "
	str += fmt.Sprint("next:", s.Next)
	return str
}

type SinglesChainSolver interface {
	SearchChain(b *Board, chain *map[*CCandidates]ChainNode, val uint8)
}

type CandidateGroup = CellCandidates.CandidateGroup
type CCandidates = CellCandidates.CellCandidates
type Board = SudokuBoard.SudokuBoard
type UnitGetter = CellCandidates.UnitGetter
type UnitsGetter = SudokuBoard.UnitsGetter

var UnitGetters = CellCandidates.UnitGetters

func (s ChainFinder) SetCandidates(solver SinglesChainSolver, b *Board, linktype func(c *CCandidates, val uint8) map[*CCandidates]LinkType) {
	for val := uint8(1); val <= 9; val++ {
		candidates := b.GetCandidateCount()
		s.SetCandidatesForValue(solver, b, val, linktype)
		if candidates != b.GetCandidateCount() {
			return
		}
	}
}

func (s ChainFinder) SetCandidatesForValue(solver SinglesChainSolver, b *Board, val uint8, linktype func(c *CCandidates, val uint8) map[*CCandidates]LinkType) {
	searched := make(map[*CCandidates]bool)

	for i := range b.Candidates {
		c := &b.Candidates[i]
		if !c.IsCandidate(val) || c.IsSolved() {
			continue
		}

		if _, ok := searched[c]; !ok {
			chain := make(map[*CCandidates]ChainNode)
			s.CreateChain(c, val, 0, &chain, linktype)
			//Add chain to searched
			for k := range chain {
				searched[k] = true
			}

			if len(chain) >= 2 {
				solver.SearchChain(b, &chain, val)
			}
		}
	}
}

func (s ChainFinder) CreateChain(c *CCandidates, val uint8, color int, chain *map[*CCandidates]ChainNode, linktype func(c *CCandidates, val uint8) map[*CCandidates]LinkType) {
	(*chain)[c] = ChainNode{color, make(map[*CCandidates]LinkType), nil}

	links := linktype(c, val)
	for k, v := range links {
		chain_node := (*chain)[c]
		chain_node.Links[k] = v
		(*chain)[c] = chain_node

		if _, ok := (*chain)[k]; !ok {
			nextcolor := (color + 1) % 2
			s.CreateChain(k, val, nextcolor, chain, linktype)
		}
	}
}

func (s ChainFinder) GetStrongLinks(c *CCandidates, val uint8) map[*CCandidates]LinkType {
	return s.getLinks(c, val, 1, 1)
}

func (s ChainFinder) GetWeakLinks(c *CCandidates, val uint8) map[*CCandidates]LinkType {
	return s.getLinks(c, val, 2, 999)
}

func (s ChainFinder) GetAllLinks(c *CCandidates, val uint8) map[*CCandidates]LinkType {
	return s.getLinks(c, val, 1, 999)
}

func (s ChainFinder) getLinks(c *CCandidates, val uint8, min_units, max_units int) map[*CCandidates]LinkType {
	links := make(map[*CCandidates]LinkType)

	for _, g := range UnitGetters {
		if unit := CellCandidates.GetUnitLinks(c, val, g); len(unit) >= min_units && len(unit) <= max_units {
			for _, l := range unit {
				if l != c {
					if len(unit) == 1 {
						links[l] = Strong
					} else if _, ok := links[l]; !ok {
						links[l] = Weak
					}
				}
			}
		}
	}
	return links
}
