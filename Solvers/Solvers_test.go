package Solvers

import (
	"SudoGoSolver/CSVReader"
	"SudoGoSolver/SudokuBoard"
	"io"
	"log"
	"os"
	"runtime"
	"sync"
	"testing"
)

func TestSolvers(t *testing.T) {
	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err.Error())
	}
	c, err := CSVReader.New(cwd + "/_test_puzzles.txt")
	if err != nil {
		log.Fatal(err.Error())
	}

	var worker_group sync.WaitGroup
	var result_group sync.WaitGroup

	queue := make(chan []*SudokuBoard.SudokuBoard, runtime.NumCPU()*2)
	done := make(chan []*SudokuBoard.SudokuBoard, runtime.NumCPU()*2)

	for i := 0; i < runtime.NumCPU(); i++ {
		worker_group.Add(1)
		go SolveAll(&worker_group, queue, done)
	}

	result_group.Add(1)
	go assertResults(&result_group, done, t)

	for {
		b, err := c.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err.Error())
		}
		queue <- b
	}
	close(queue)
	worker_group.Wait()
	close(done)
	result_group.Wait()
}

func assertResults(wg *sync.WaitGroup, done chan []*SudokuBoard.SudokuBoard, t *testing.T) {
	defer wg.Done()

	for b := range done {
		if !b[0].Equals(b[1]) {
			t.Errorf("Solved board %s different from expected solution %s", b[0], b[1])
		}
	}
}
