package Solvers

import (
	"SudoGoSolver/CellCandidates"
)

type PointingPair struct{}

func (s PointingPair) getOtherGroups(group *CandidateGroup, cell *CCandidates) (*CandidateGroup, *CandidateGroup) {
	switch group {
	case cell.GetBox():
		return cell.GetColumn(), cell.GetRow()
	case cell.GetRow():
		return cell.GetColumn(), cell.GetBox()
	default:
		return cell.GetRow(), cell.GetBox()
	}
}

func (s PointingPair) resolvePair(group *CandidateGroup, cells CandidateGroup, val uint8) {
	for _, c := range *group {
		if !CellCandidates.GroupContains(cells, c) {
			c.RemoveCandidate(val)
		}
	}
}

func (s PointingPair) SetCandidatesInGroupN(group *CandidateGroup, group_size int) {
	for val := uint8(1); val <= 9; val++ {
		containing_cells := make([]*CCandidates, 0, 9)
		for i := 0; i < 9; i++ {
			if (*group)[i].IsCandidate(val) {
				containing_cells = append(containing_cells, (*group)[i])
			}
			if len(containing_cells) > group_size {
				break
			}
		}
		if len(containing_cells) == group_size {
			g1, g2 := s.getOtherGroups(group, containing_cells[0])
			if CellCandidates.IsSubGroup(*g1, containing_cells) {
				s.resolvePair(g1, containing_cells, val)
			} else if CellCandidates.IsSubGroup(*g2, containing_cells) {
				s.resolvePair(g2, containing_cells, val)
			}
		}
	}
}

func (s PointingPair) SetCandidatesInGroup(group *CandidateGroup) {
	s.SetCandidatesInGroupN(group, 2)
	s.SetCandidatesInGroupN(group, 3)
}

func (s PointingPair) SetCandidates(b *Board) {
	SetCandidates(s, b)
}
