package Solvers

import (
	"SudoGoSolver/CellCandidates"
	"SudoGoSolver/SudokuBoard"
	"reflect"
	"sync"
)

type CandidateGroup = CellCandidates.CandidateGroup
type CCandidates = CellCandidates.CellCandidates
type UnitGetter = CellCandidates.UnitGetter
type UnitsGetter = SudokuBoard.UnitsGetter
type Board = SudokuBoard.SudokuBoard

var UnitGetters = CellCandidates.UnitGetters

type Solver interface {
	SetCandidates(b *Board)
}

type GroupSolver interface {
	SetCandidatesInGroup(group *CandidateGroup)
}

func ReverseSlice(s interface{}) {
	size := reflect.ValueOf(s).Len()
	swap := reflect.Swapper(s)
	for i, j := 0, size-1; i < j; i, j = i+1, j-1 {
		swap(i, j)
	}
}

func SetCandidates(s GroupSolver, b *Board) {
	for i := 0; i < 9; i++ {
		s.SetCandidatesInGroup(&b.Rows[i])
		s.SetCandidatesInGroup(&b.Columns[i])
		s.SetCandidatesInGroup(&b.Boxes[i])
	}
}

func doItteration(b *Board, solvers []Solver) bool {
	for _, solver := range solvers {
		candidates := b.GetCandidateCount()
		solver.SetCandidates(b)
		if candidates != b.GetCandidateCount() {
			// fmt.Println("Solver", fmt.Sprintf("%T", solver), "Eliminated", candidates-b.GetCandidateCount(), "Candidates")
			// fmt.Println(b)
			// fmt.Println(b.GetPrettyStringWithCandidates())
			return true
		}
	}
	return false
}

func SolveAll(wg *sync.WaitGroup, queue chan []*Board, done chan []*Board) {
	defer wg.Done()

	for l := range queue {
		for _, b := range l {
			Solve(b)
		}
		done <- l
	}
}

func Solve(b *Board) int {
	solvers := make([]Solver, 0)
	solvers = append(solvers, NakedSingle{})
	solvers = append(solvers, HiddenSingle{})
	solvers = append(solvers, NakedGroup{})
	solvers = append(solvers, HiddenGroup{})
	solvers = append(solvers, PointingPair{})
	solvers = append(solvers, BoxLineReduction{})
	solvers = append(solvers, XWing{})
	solvers = append(solvers, SinglesChainSolver{})
	solvers = append(solvers, YWing{})
	solvers = append(solvers, SwordFish{})
	solvers = append(solvers, XYZWing{})
	solvers = append(solvers, XCycles{})
	solvers = append(solvers, JellyFish{})

	var itterations int
	for {
		itterations++
		if !doItteration(b, solvers) {
			break
		}
	}

	return itterations
}
