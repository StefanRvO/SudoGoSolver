package SudokuBoard

import (
	"SudoGoSolver/CellCandidates"
	"errors"
	"strconv"
	"strings"
)

type CandidateGroup = CellCandidates.CandidateGroup
type UnitsGetter = func(c *SudokuBoard) []CandidateGroup

type SudokuBoard struct {
	Original   string
	Candidates []CellCandidates.CellCandidates
	Rows       []CandidateGroup
	Columns    []CandidateGroup
	Boxes      []CandidateGroup
}

func New(board string) (*SudokuBoard, error) {
	if len(board) < 81 {
		return nil, errors.New("board string too short")
	}
	var b SudokuBoard
	b.Original = board
	b.Candidates = make([]CellCandidates.CellCandidates, 9*9)
	b.Rows = make([]CandidateGroup, 9)
	b.Columns = make([]CandidateGroup, 9)
	b.Boxes = make([]CandidateGroup, 9)

	for i := 0; i < 9; i++ {
		b.Rows[i] = make(CandidateGroup, 9)
		b.Boxes[i] = make(CandidateGroup, 9)
		b.Columns[i] = make(CandidateGroup, 9)

	}

	for i := 0; i < 9*9; i++ {
		y := i / 9
		x := i - (y * 9)
		b.Candidates[i] = CellCandidates.New(uint8(board[i]-'0'), x, y)

	}

	// Set row & column pointers
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			idx := j + i*9
			b.Rows[i][j] = &b.Candidates[idx]
			b.Columns[j][i] = &b.Candidates[idx]

			b.Candidates[idx].SetRow(&b.Rows[i])
			b.Candidates[idx].SetColumn(&b.Columns[j])
		}
	}

	//Set box pointers
	for box := 0; box < 9; box++ {
		box_start_x := (box % 3) * 3
		box_start_y := (box / 3) * 3

		for x := box_start_x; x < box_start_x+3; x++ {
			for y := box_start_y; y < box_start_y+3; y++ {
				idx := x + y*9
				b.Boxes[box][(y-box_start_y)*3+(x-box_start_x)] = &b.Candidates[idx]
				b.Candidates[idx].SetBox(&b.Boxes[box])
			}
		}
	}

	return &b, nil
}

func (b *SudokuBoard) GetString() string {
	var board_str string

	for _, c := range b.Candidates {
		board_str += strconv.Itoa(int(c.GetValue()))
	}
	return strings.Replace(board_str, "0", ".", -1)
}

func (b *SudokuBoard) GetPrettyString() string {
	var board_str string

	for y := 0; y < 9; y++ {
		if y%3 == 0 && y != 0 {
			board_str += "---------------------\n"
		}
		for x := 0; x < 9; x++ {
			if x%3 == 0 && x != 0 {
				board_str += "| "
			}

			val := b.Candidates[x+y*9].GetValue()
			if val == 0 {
				board_str += "  "
			} else {
				board_str += strconv.Itoa(int(val)) + " "
			}
		}
		board_str += "\n"
	}

	return board_str
}

func (b *SudokuBoard) GetPrettyStringWithCandidates() string {
	lines := make([]string, 35)
	l := 0
	for row_idx, row := range b.Rows {

		candidate_lines := make([][]string, 9)
		for i, c := range row {
			candidate_lines[i] = c.GetPrettyString()
		}

		for y := 0; y < 3; y++ {
			for x, cl := range candidate_lines {
				lines[l] += cl[y]
				if x != 8 {
					if (x+1)%3 == 0 {
						lines[l] += " || "
					} else {
						lines[l] += " | "
					}
				}
			}
			l++
		}
		if row_idx != 8 {
			if (row_idx+1)%3 == 0 {
				lines[l] = strings.Repeat(":", len(lines[0]))
				l++
			} else {
				lines[l] = strings.Repeat("-", len(lines[0]))
				l++
			}
		}
	}

	return strings.Join(lines, "\n")
}

func (b *SudokuBoard) GetCandidateCount() int {
	sum := 0
	for _, c := range b.Candidates {
		sum += c.GetCandidateCount()
	}
	return sum
}

func (b *SudokuBoard) IsSolved() bool {
	return b.GetCandidateCount() == 81
}

func (b *SudokuBoard) Equals(cmp *SudokuBoard) bool {
	for i, _ := range b.Candidates {
		if b.Candidates[i].GetValue() != cmp.Candidates[i].GetValue() {
			return false
		}
	}
	return true
}

func (b *SudokuBoard) String() string {
	return b.GetString()
}

func GetRows(s *SudokuBoard) []CandidateGroup {
	return s.Rows
}

func GetColumns(s *SudokuBoard) []CandidateGroup {
	return s.Columns
}

func GetBoxs(s *SudokuBoard) []CandidateGroup {
	return s.Boxes
}
