package CellCandidates

import (
	"fmt"
	"math/bits"
	"strconv"

	"gonum.org/v1/gonum/stat/combin"
)

type CellCandidates struct {
	Candidates uint32
	X          int
	Y          int
	row        *CandidateGroup
	column     *CandidateGroup
	box        *CandidateGroup
}

type CandidateGroup = []*CellCandidates
type UnitGetter = func(c *CellCandidates) *CandidateGroup

var UnitGetters = []UnitGetter{GetBox, GetRow, GetColumn}

type FilterFunc = func(c *CellCandidates) bool

func GroupContains(g CandidateGroup, c *CellCandidates) bool {
	for _, v := range g {
		if v == c {
			return true
		}
	}
	return false
}

func IsSubGroup(g CandidateGroup, c CandidateGroup) bool {
	for _, v := range c {
		if !GroupContains(g, v) {
			return false
		}
	}
	return true
}

func GetCandidateCount(val uint32) int {
	return bits.OnesCount32(val)
}

func GetCellsWithCandidates(g CandidateGroup, val uint32) CandidateGroup {
	cells := make(CandidateGroup, 0)
	for _, c := range g {
		if !c.IsSolved() && (c.Candidates|val) == c.Candidates {
			cells = append(cells, c)
		}
	}
	return cells
}

func RemoveCandidatesFromGroup(g CandidateGroup, exclude CandidateGroup, candidates uint32) {
	for _, c := range g {
		if !GroupContains(exclude, c) {
			c.RemoveCandidates(candidates)
		}
	}
}

func GetUnitLinks(c *CellCandidates, val uint8, unit UnitGetter) CandidateGroup {
	var g CandidateGroup
	for _, l := range *unit(c) {
		if l.IsCandidate(val) && l != c {
			g = append(g, l)
		}
	}
	return g
}

func FilterGroup(g CandidateGroup, f FilterFunc) CandidateGroup {
	filtered := make(CandidateGroup, 0, len(g))
	for _, c := range g {
		if f(c) {
			filtered = append(filtered, c)
		}
	}
	return filtered
}

func New(val uint8, x, y int) CellCandidates {
	var c CellCandidates
	c.X = x
	c.Y = y

	if val <= 9 && val > 0 {
		c.Candidates |= 1 << val
	} else {
		for i := uint8(1); i <= 9; i++ {
			c.Candidates |= 1 << i
		}
	}
	return c
}

func (c *CellCandidates) GetCommonCellsInUnits(other *CellCandidates, u1, u2 UnitGetter) CandidateGroup {
	g1 := u1(c)
	g2 := u2(other)
	common := make(CandidateGroup, 0, 2)

	for _, cell := range *g1 {
		if GroupContains(*g2, cell) {
			common = append(common, cell)
		}
	}
	return common
}

func (c *CellCandidates) GetCommonCells(other *CellCandidates) CandidateGroup {
	common_map := make(map[*CellCandidates]bool)

	for _, g1 := range UnitGetters {
		for _, g2 := range UnitGetters {
			for _, cell := range c.GetCommonCellsInUnits(other, g1, g2) {
				common_map[cell] = true
			}
		}
	}

	common := make(CandidateGroup, 0, 8)
	for k, _ := range common_map {
		common = append(common, k)
	}
	return common
}

func (c *CellCandidates) IsSolved() bool {
	return c.GetCandidateCount() == 1
}

func (c *CellCandidates) RemoveCandidate(val uint8) {
	c.Candidates &= ^(1 << val)
}

func (c *CellCandidates) RemoveCandidates(val uint32) {
	c.Candidates &= (^val)
}

func (c *CellCandidates) Set(Candidates uint32) {
	c.Candidates = Candidates
}

func (c *CellCandidates) SetCandidate(candidate uint8) {
	c.Candidates = 1 << candidate
}

func (c *CellCandidates) SetCandidates(Candidates []uint8) {
	c.Candidates = 0
	for _, val := range Candidates {
		c.Candidates |= 1 << val
	}
}

func (c *CellCandidates) AddCandidate(val uint8) {
	c.Candidates |= 1 << val
}

func (c *CellCandidates) GetValue() uint8 {
	if !c.IsSolved() {
		return 0
	}
	return uint8(bits.TrailingZeros32(c.Candidates))
}

func (c *CellCandidates) GetCandidateCount() int {
	return bits.OnesCount32(c.Candidates)
}

func (c *CellCandidates) GetCandidates() []uint8 {
	candidates := make([]uint8, 0, 9)
	for i := uint8(1); i <= 9; i++ {
		if (c.Candidates & (1 << i)) > 0 {
			candidates = append(candidates, i)
		}
	}
	return candidates
}

func GetCandidates(val uint32) []uint8 {
	candidates := make([]uint8, 0, 9)
	for i := uint8(1); i <= 9; i++ {
		if (val & (1 << i)) > 0 {
			candidates = append(candidates, i)
		}
	}
	return candidates
}

func (c *CellCandidates) IsCandidate(val uint8) bool {
	return c.Candidates&(1<<val) != 0
}

func (c *CellCandidates) IsCandidates(val uint32) bool {
	return c.Candidates == (c.Candidates | val)
}

func (c *CellCandidates) String() string {
	x := c.X + 1
	y := 'A' + c.Y
	return fmt.Sprintf("%s(%c%d)", fmt.Sprint(c.GetCandidates()), y, x)
}

func (c *CellCandidates) GetPrettyString() []string {
	lines := make([]string, 3)
	for l := 0; l < 3; l++ {
		for x := 1; x <= 3; x++ {
			v := l*3 + x
			if c.IsSolved() {
				lines[l] += strconv.Itoa(int(c.GetValue()))
			} else if c.IsCandidate(uint8(v)) {
				lines[l] += strconv.Itoa(v)
			} else {
				lines[l] += " "
			}
			if x != 3 {
				lines[l] += " "
			}
		}
	}
	return lines
}

func GetCandidatesPermutations(c uint32, group_size int) []uint32 {
	candidates := GetCandidates(c)
	var permutations []uint32
	if len(candidates) < group_size {
		return permutations
	}

	for _, comb := range combin.Combinations(len(candidates), group_size) {
		var perm uint32
		for _, i := range comb {
			perm |= 1 << candidates[i]
		}
		permutations = append(permutations, perm)
	}
	return permutations
}

func GetCandidatePermutations(c uint32, group_size int) [][]uint8 {
	candidates := GetCandidates(c)
	var permutations [][]uint8
	if len(candidates) < group_size {
		return permutations
	}

	for _, comb := range combin.Permutations(len(candidates), group_size) {
		var perm []uint8
		for _, i := range comb {
			perm = append(perm, candidates[i])
		}
		permutations = append(permutations, perm)
	}
	return permutations
}

func (c *CellCandidates) GetRow() *CandidateGroup {
	return c.row
}

func (c *CellCandidates) GetColumn() *CandidateGroup {
	return c.column
}

func (c *CellCandidates) GetBox() *CandidateGroup {
	return c.box
}

func GetRow(c *CellCandidates) *CandidateGroup {
	return c.GetRow()
}

func GetColumn(c *CellCandidates) *CandidateGroup {
	return c.GetColumn()
}

func GetBox(c *CellCandidates) *CandidateGroup {
	return c.GetBox()
}

func (c *CellCandidates) SetRow(r *CandidateGroup) {
	c.row = r
}

func (c *CellCandidates) SetColumn(column *CandidateGroup) {
	c.column = column
}

func (c *CellCandidates) SetBox(b *CandidateGroup) {
	c.box = b
}
