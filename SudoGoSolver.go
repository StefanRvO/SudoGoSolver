package main

import (
	"SudoGoSolver/CSVReader"
	"SudoGoSolver/Solvers"
	"SudoGoSolver/SudokuBoard"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"runtime/pprof"
	"sync"
)

func main() {
	board := flag.String("board", "", "Board string")
	solution := flag.String("solution", "", "Solution string")
	csv := flag.String("csv", "", "path to csv with boards and solutions")
	cpuprofile := flag.String("cpuprofile", "", "write cpu profile to file")

	flag.Parse()

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if *csv != "" {
		SolveCSV(*csv)
	}

	if *board != "" {
		SolveBoard(*board, *solution)
	}

}

func SolveCSV(path string) {
	c, err := CSVReader.New(path)
	if err != nil {
		log.Fatal(err.Error())
	}

	var worker_group sync.WaitGroup
	var result_group sync.WaitGroup

	queue := make(chan []*SudokuBoard.SudokuBoard, runtime.NumCPU()*2)
	done := make(chan []*SudokuBoard.SudokuBoard, runtime.NumCPU()*2)

	for i := 0; i < runtime.NumCPU(); i++ {
		worker_group.Add(1)
		go Solvers.SolveAll(&worker_group, queue, done)
	}

	result_group.Add(1)
	go SolveCSVResults(&result_group, done)

	for {
		b, err := c.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err.Error())
		}
		queue <- b
	}
	close(queue)
	worker_group.Wait()
	close(done)
	result_group.Wait()

	os.Exit(0)
}

func SolveCSVResults(wg *sync.WaitGroup, done chan []*SudokuBoard.SudokuBoard) {
	defer wg.Done()

	total := 0
	solved := 0
	for b := range done {
		total++
		if len(b) >= 2 && !b[0].Equals(b[1]) {
			log.Println("Puzzle", total, b[0], "Is not equal to", b[1], "original:", b[0].Original)
		} else if len(b) >= 1 && !b[0].IsSolved() {
			log.Println("Puzzle", total, b[0], "Is not solved, original:", b[0].Original)
		} else {
			// fmt.Printf("%s,%s\n", fmt.Sprint(b[0].Original), fmt.Sprint(b[0]))
			solved++
		}
	}

	log.Printf("Solved/Total: %d/%d (%f %%)", solved, total, float64(solved)/float64(total)*100)
}

func SolveBoard(board string, solution string) {
	if board == "" {
		log.Fatal("No board string provided!")
	}

	b, err := SudokuBoard.New(board)
	if err != nil {
		log.Fatal("Error when creating board:", err)
	}
	fmt.Println(b.GetPrettyString())

	fmt.Println("Itterations:", Solvers.Solve(b))
	fmt.Println("Candidates:", b.GetCandidateCount())
	fmt.Println()

	fmt.Println(b.GetPrettyString())

	fmt.Println(b)

	if solution != "" {
		s, err := SudokuBoard.New(solution)
		if err != nil || !b.Equals(s) {
			log.Println("solution does not match")
		}
	}

}
