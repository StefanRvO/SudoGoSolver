package CSVReader

import (
	"SudoGoSolver/SudokuBoard"
	"encoding/csv"
	"errors"
	"fmt"
	"os"
)

type CSVReader struct {
	reader *csv.Reader
	len    int
}

func New(path string) (*CSVReader, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, errors.New("Could not open " + path + " : " + err.Error())
	}
	reader := csv.NewReader(file)
	record, _ := reader.Read() //Read first line with headers

	return &CSVReader{reader, len(record)}, nil
}

func (c *CSVReader) Next() ([]*SudokuBoard.SudokuBoard, error) {
	record, err := c.reader.Read()
	boards := make([]*SudokuBoard.SudokuBoard, 0)
	if err != nil {
		return boards, err
	}
	if len(record) < 1 {
		return boards, errors.New(fmt.Sprint("Record (", record, ") is empty"))
	}

	for i := 0; len(boards) < 1 || (len(record) >= 2 && len(boards) < 2); i++ {
		b, err := SudokuBoard.New(record[i])
		if err != nil {
			return boards, err
		}
		boards = append(boards, b)

	}

	return boards, nil
}
